# README #

### delivery-route ###
Web Service in Rest to help you choose the better route. It receives a map with some coordinates between two points, persist this with the name of map and calculate the better route and cost of fuel between two points of this map.

### Solution ###
* Spring, Jetty and MongoDB. I chose Spring because it is very simple to work and fits well for this requirements. About Jetty, it is a fast and simple server that is easy to configure and more productive when it works with Eclipse. MongoDB was a personal choise to practice a little more in this database. 

### System requirements ###
* Java7, Maven 3, MongoDB, some REST client or curl app

### Set up ###
* Dependencies
  Declared inside the pom.xml
* Maven configuration
  - Download Maven 3 here:https://maven.apache.org/download.cgi
  - Install and configure following this instrucions: https://maven.apache.org/install.html and https://maven.apache.org/configure.html
* Database configuration
  - Download MongoDB here: www.mongodb.org/downloads
  - Install and start the server. Instruction here: http://docs.mongodb.org/manual/
  - Run mongoDB and create a database named routedb (use the command "use routedb")
* Unit tests
  To run tests: mvn clean install
* Deployment instructions - 
  mvn jetty:run

### Running ###

* I used PostMan, a REST client

### SAVE MAP - Put, Body, raw, Json ###
![saveMap](https://bytebucket.org/AlissonBertho/delivey-route/raw/003a4b2a943b29eb90c3dceedffe19a394bce58e/saveMap.png?token=a2f3f8d1e7202b3c36251a6d2ff9ee37038ca868)
url: localhost:8080/routeService/saveMap

### FIND MAP - Get, Body ###
![findByMapName](https://bytebucket.org/AlissonBertho/delivey-route/raw/003a4b2a943b29eb90c3dceedffe19a394bce58e/findByMapName.png?token=3482808a5e957ade9da32d98ff121f216320e880)
url: localhost:8080/routeService/findByMapName?mapName=SP

### CALCULATE BETTER ROUTE - Get, Body ### 
![calculateBetterRoute](https://bytebucket.org/AlissonBertho/delivey-route/raw/003a4b2a943b29eb90c3dceedffe19a394bce58e/calculateBetterRoute.png?token=e906996fe0ea2ce52a772e2fd4d1492a0bd1bf61)
url: localhost:8080/routeService/calculateBetterRoute?mapName=SP&startPoint=A&endPoint=D&fuelPrice=2.5&autonomy=10.0

### CLEAN MAP - Delete, Body, raw ###
![cleanMap](https://bytebucket.org/AlissonBertho/delivey-route/raw/003a4b2a943b29eb90c3dceedffe19a394bce58e/cleanMap.png?token=72b4bb2c630f132ab01e58ff7f0e14a58706bc15)
url: localhost:8080/routeService/cleanMap?mapName=SP