package com.walmart.deliveryroute.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author Alisson
 *
 */
@ImportResource(value = {"classpath:SpringConfig.xml"})
@Configuration
@EnableWebMvc
@ComponentScan(basePackages="com.walmart.deliveryroute") 
public class WebMvcConfig {

}
