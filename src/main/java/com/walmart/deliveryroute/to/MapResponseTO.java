/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alisson
 *
 */
public class MapResponseTO extends BaseResponseTO implements Serializable {

	public MapResponseTO(Integer code) {
		super(code);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -295449227646715594L;
	private List<String> coordinates = new ArrayList<String>();

	public List<String> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<String> coordinates) {
		this.coordinates = coordinates;
	}
}
