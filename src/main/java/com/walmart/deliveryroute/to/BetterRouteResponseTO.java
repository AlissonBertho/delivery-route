/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.to;

import java.io.Serializable;

/**
 * @author Alisson
 *
 */
public class BetterRouteResponseTO extends BaseResponseTO implements Serializable {

	public BetterRouteResponseTO(Integer code) {
		super(code);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5748453878903898759L;
	private String locations;
	private Double betterCost;

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public Double getBetterCost() {
		return betterCost;
	}

	public void setBetterCost(Double betterCost) {
		this.betterCost = betterCost;
	}
}
