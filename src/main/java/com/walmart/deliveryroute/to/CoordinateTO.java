/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.to;

import java.io.Serializable;

/**
 * @author Alisson
 *
 */
public class CoordinateTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1975335619313194702L;
	private String startPoint;
	private String endPoint;
	private Double distance;
	
	public CoordinateTO(){
	}
	
	public CoordinateTO(String from, String to, Double distance){
		this.startPoint = from;
		this.endPoint = to;
		this.distance = distance;
	}

	public String getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}
}
