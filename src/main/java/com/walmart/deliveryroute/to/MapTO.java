/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.to;

import java.io.Serializable;
import java.util.List;

/**
 * @author Alisson
 *
 */
public class MapTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5862781189186100544L;
	private String name;
	private List<CoordinateTO> coordinates;
	
	public MapTO() {
	}
	
	public MapTO(String stateName, List<CoordinateTO> routes) {
		this.name = stateName;
		this.coordinates = routes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CoordinateTO> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<CoordinateTO> coordinates) {
		this.coordinates = coordinates;
	}
}
