/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.service;

import static com.walmart.deliveryroute.to.BaseResponseTO.ERROR;
import static com.walmart.deliveryroute.to.BaseResponseTO.SUCCESS;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.deliveryroute.bo.CoordinateBO;
import com.walmart.deliveryroute.bo.MapBO;
import com.walmart.deliveryroute.entity.Coordinate;
import com.walmart.deliveryroute.to.BaseResponseTO;
import com.walmart.deliveryroute.to.BetterRouteResponseTO;
import com.walmart.deliveryroute.to.MapResponseTO;
import com.walmart.deliveryroute.to.MapTO;

/**
 * @author Alisson
 *
 */
@RestController
@RequestMapping(value = "/routeService")
public class RouteService {

	@Autowired
	private MapBO routeBO;

	@Autowired
	private CoordinateBO coordinateBO;

	@RequestMapping(value = "/saveMap", method = RequestMethod.PUT, headers = { "Accept=application/json", "content-type=application/json" })
	public BaseResponseTO saveMap(@RequestBody final MapTO mapTO) {
		BaseResponseTO baseResponseTO;
		try {
			baseResponseTO = routeBO.saveMap(mapTO);
		} catch (Exception e) {
			baseResponseTO = new BaseResponseTO(ERROR);
		}
		return baseResponseTO;
	}

	@RequestMapping(value = "/findByMapName", method = RequestMethod.GET)
	public MapResponseTO findByMapName(@RequestParam(value = "mapName", required = true) String mapName) {
		MapResponseTO mapResponseTO = new MapResponseTO(SUCCESS);
		try {
			List<Coordinate> coordinates = coordinateBO.findByMapName(mapName);
			for (Coordinate coordinate : coordinates) {
				mapResponseTO.getCoordinates().add(coordinate.toString());
			}
		} catch (Exception e) {
			mapResponseTO = new MapResponseTO(ERROR);
		}
		return mapResponseTO;
	}

	@RequestMapping(value = "/calculateBetterRoute", method = RequestMethod.GET)
	public BetterRouteResponseTO calculateBetterRoute(@RequestParam(value = "mapName", required = true) String mapName,
			@RequestParam(value = "startPoint", required = true) String startPoint, @RequestParam(value = "endPoint", required = true) String endPoint,
			@RequestParam(value = "fuelPrice", required = true) Double fuelPrice, @RequestParam(value = "autonomy", required = true) Double autonomy) {
		BetterRouteResponseTO betterRouteResponseTO = new BetterRouteResponseTO(SUCCESS);
		try {
			betterRouteResponseTO = routeBO.calculateBetterRoute(mapName, startPoint, endPoint, fuelPrice, autonomy);
		} catch (Exception e) {
			betterRouteResponseTO = new BetterRouteResponseTO(ERROR);
		}
		return betterRouteResponseTO;
	}

	@RequestMapping(value = "/cleanMap", method = RequestMethod.DELETE)
	public MapResponseTO cleanMap(@RequestParam(value = "mapName", required = true) String mapName) {
		MapResponseTO mapResponseTO = new MapResponseTO(SUCCESS);
		try {
			routeBO.cleanMap(mapName);
		} catch (Exception e) {
			mapResponseTO = new MapResponseTO(ERROR);
		}
		return mapResponseTO;
	}
}
