/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.walmart.deliveryroute.entity.Coordinate;

/**
 * @author Alisson
 *
 */
@Repository
public interface CoordinateRepository extends MongoRepository<Coordinate, String> {

	@Query("{mapName : ?0}")
	public List<Coordinate> findByMapName(String name);
}
