/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.deliveryroute.entity.Location;
import com.walmart.deliveryroute.repository.LocationRepository;

/**
 * @author Alisson
 *
 */
@Service
@Transactional
public class LocationBO {

	@Autowired
	private LocationRepository locationRepository;

	@Transactional
	public Location saveLocation(String name, String mapName) {
		Location location = locationRepository.findByName(name);
		if (location != null) {
			return location;
		}
		return locationRepository.save(new Location(name, mapName));
	}

	@Transactional
	public Location saveLocation(Location Location) {
		return locationRepository.save(Location);
	}

	@Transactional
	public void deleteLocation(Location location) {
		locationRepository.delete(location);
	}
	
	@Transactional
	public List<Location> findByMapName(String mapName) {
		return locationRepository.findByMapName(mapName);
	}

//	public Location findByName(String name){
//		return locationRepository.findByName(name);
//	}

	@Transactional
	public void deleteByMapName(String mapName) {
		List<Location> locations = locationRepository.findByMapName(mapName);
		for (Location location : locations) {
			locationRepository.delete(location);
		}
	}
}
