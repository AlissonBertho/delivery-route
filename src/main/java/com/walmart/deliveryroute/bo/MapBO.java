/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.bo;

import static com.walmart.deliveryroute.to.BaseResponseTO.SUCCESS;
import static org.apache.commons.lang.math.NumberUtils.INTEGER_ZERO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.deliveryroute.entity.Coordinate;
import com.walmart.deliveryroute.entity.Location;
import com.walmart.deliveryroute.to.BaseResponseTO;
import com.walmart.deliveryroute.to.BetterRouteResponseTO;
import com.walmart.deliveryroute.to.CoordinateTO;
import com.walmart.deliveryroute.to.MapTO;

import es.usc.citius.hipster.algorithm.Algorithm.SearchResult;
import es.usc.citius.hipster.algorithm.Hipster;
import es.usc.citius.hipster.graph.GraphBuilder;
import es.usc.citius.hipster.graph.GraphSearchProblem;
import es.usc.citius.hipster.model.impl.WeightedNode;
import es.usc.citius.hipster.model.problem.SearchProblem;

/**
 * @author Alisson
 *
 */
@Service
public class MapBO {

	@Autowired
	private LocationBO locationBO;

	@Autowired
	private CoordinateBO coordinateBO;

	@Transactional
	public BaseResponseTO saveMap(MapTO mapTO) {

		for (CoordinateTO coordinate : mapTO.getCoordinates()) {

			saveRoute(coordinate, mapTO.getName());
		}
		return new BaseResponseTO(BaseResponseTO.SUCCESS);
	}

	private void saveRoute(CoordinateTO route, String mapName) {

		Location start = locationBO.saveLocation(new Location(route.getStartPoint(), mapName));
		Location end = locationBO.saveLocation(new Location(route.getEndPoint(), mapName));

		Coordinate coordinate = new Coordinate(start, end, route.getDistance(), mapName);
		coordinateBO.saveCoordinate(coordinate);
	}

	@Transactional
	public void cleanMap(String mapName) {
		locationBO.deleteByMapName(mapName);
		coordinateBO.deleteByMapName(mapName);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BetterRouteResponseTO calculateBetterRoute(String mapName, String startPoint, String endPoint, Double fuelPrice, Double autonomy) {

		List<Coordinate> coordinates = coordinateBO.findByMapName(mapName);

		SearchResult searchResult = applyDijkstraAlgorithm(startPoint, endPoint, coordinates);

		String betterRoute = searchResult.getOptimalPaths().toString();

		List<WeightedNode> nodes = new ArrayList<WeightedNode>(searchResult.getGoalNodes());

		Double distance = Double.valueOf(nodes.get(INTEGER_ZERO).getCost().toString());

		BetterRouteResponseTO betterRouteResponseTO = new BetterRouteResponseTO(SUCCESS);
		betterRouteResponseTO.setLocations(betterRoute);
		betterRouteResponseTO.setBetterCost(calculateBetterCost(fuelPrice, autonomy, distance));

		return betterRouteResponseTO;
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	private SearchResult applyDijkstraAlgorithm(String startPoint, String endPoint, List<Coordinate> coordinates) {
		GraphBuilder<Object, Object> graph = GraphBuilder.create();
		for (Coordinate coordinate : coordinates) {
			graph.connect(coordinate.getStartPoint().getName()).to(coordinate.getEndPoint().getName()).withEdge(coordinate.getDistance());
		}

		SearchProblem searchProblem = GraphSearchProblem.startingFrom(startPoint).in(graph.buildDirectedGraph()).takeCostsFromEdges().build();

		SearchResult searchResult = Hipster.createDijkstra(searchProblem).search(endPoint);
		return searchResult;
	}

	private double calculateBetterCost(Double fuelPrice, Double autonomy, Double distance) {
		return distance / autonomy * fuelPrice;
	}
}
