/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.deliveryroute.entity.Coordinate;
import com.walmart.deliveryroute.repository.CoordinateRepository;

/**
 * @author Alisson
 *
 */
@Service
@Transactional
public class CoordinateBO {

	@Autowired
	private CoordinateRepository coordinateRepository;

	@Transactional
	public Coordinate saveCoordinate(Coordinate Coordinate) {
		return coordinateRepository.save(Coordinate);
	}
	
	@Transactional
	public List<Coordinate> findByMapName(String mapName) {
		return coordinateRepository.findByMapName(mapName);
	}
	
	@Transactional
	public void deleteByMapName(String mapName) {
		List<Coordinate> coordinates = coordinateRepository.findByMapName(mapName);
		for (Coordinate coordinate : coordinates) {
			coordinateRepository.delete(coordinate);
		}
	}
}
