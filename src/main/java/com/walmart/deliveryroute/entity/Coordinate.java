/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Alisson
 *
 */
@Document(collection = "coordinates")
public class Coordinate {

	@Id
	private String id;

	private String mapName;

	private Location startPoint;

	private Location endPoint;

	private Double distance;

	public Coordinate(Location startPoint, Location endPoint, Double distance, String mapName) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.distance = distance;
		this.mapName = mapName;
	}

	public Location getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Location startPoint) {
		this.startPoint = startPoint;
	}

	public Location getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(Location endPoint) {
		this.endPoint = endPoint;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "{startPoint: " + startPoint.getName() + ", endPoint: " + endPoint.getName() + ", distance: " + distance + "}";
	}
}
