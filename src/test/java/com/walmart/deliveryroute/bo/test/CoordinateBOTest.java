/*
 *
 * This file is a confidential property of Walmart Test. No part of this
 * file may be reproduced or copied.
 *
 */
package com.walmart.deliveryroute.bo.test;

import static com.walmart.deliveryroute.mock.LocationMock.A;
import static com.walmart.deliveryroute.mock.LocationMock.B;
import static com.walmart.deliveryroute.mock.LocationMock.DEZ;
import static com.walmart.deliveryroute.mock.LocationMock.MAP;
import static org.apache.commons.lang.math.NumberUtils.INTEGER_ONE;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.walmart.deliveryroute.bo.CoordinateBO;
import com.walmart.deliveryroute.entity.Coordinate;
import com.walmart.deliveryroute.entity.Location;
import com.walmart.deliveryroute.mock.CoordinateMock;
import com.walmart.deliveryroute.repository.CoordinateRepository;

/**
 * @author Alisson
 *
 */
public class CoordinateBOTest {

	@InjectMocks
	private CoordinateBO coordinateBO;

	@Mock
	private CoordinateRepository coordinateRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Coordinate coordinate = new CoordinateMock().createMock();
		when(coordinateRepository.save(Mockito.any(Coordinate.class))).thenReturn(coordinate);
		when(coordinateRepository.findByMapName(MAP)).thenReturn(new CoordinateMock().createMockList());
	}

	@Test
	public void saveCoordinateTest() {
		Coordinate coordinate = new Coordinate(new Location(A, MAP), new Location(B, MAP), DEZ, MAP);
		Coordinate coordinateResponse = coordinateRepository.save(coordinate);

		Assert.assertEquals(coordinateResponse.getMapName(), MAP);
		Assert.assertEquals(coordinateResponse.getStartPoint().getName(), A);
		Assert.assertEquals(coordinateResponse.getEndPoint().getName(), B);
	}

	@Test
	public void findByMapNameTest() {
		List<Coordinate> coordinates = coordinateRepository.findByMapName(MAP);
		Assert.assertNotNull(coordinates);
		Assert.assertTrue(!coordinates.isEmpty());
		Assert.assertEquals(coordinates.get(INTEGER_ONE).getStartPoint().getName(), B);
	}
}
