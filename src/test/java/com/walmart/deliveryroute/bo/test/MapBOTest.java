package com.walmart.deliveryroute.bo.test;

import static com.walmart.deliveryroute.mock.LocationMock.A;
import static com.walmart.deliveryroute.mock.LocationMock.D;
import static com.walmart.deliveryroute.mock.LocationMock.DEZ;
import static com.walmart.deliveryroute.mock.LocationMock.MAP;
import static com.walmart.deliveryroute.to.BaseResponseTO.MSG_SUCCESS;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.walmart.deliveryroute.bo.CoordinateBO;
import com.walmart.deliveryroute.bo.LocationBO;
import com.walmart.deliveryroute.bo.MapBO;
import com.walmart.deliveryroute.entity.Coordinate;
import com.walmart.deliveryroute.entity.Location;
import com.walmart.deliveryroute.mock.CoordinateMock;
import com.walmart.deliveryroute.mock.LocationMock;
import com.walmart.deliveryroute.repository.LocationRepository;
import com.walmart.deliveryroute.to.BaseResponseTO;
import com.walmart.deliveryroute.to.BetterRouteResponseTO;
import com.walmart.deliveryroute.to.CoordinateTO;
import com.walmart.deliveryroute.to.MapTO;

public class MapBOTest {

	@InjectMocks
	private MapBO mapBO;

	@Mock
	private LocationBO locationBO;

	@Mock
	private CoordinateBO coordinateBO;

	@Mock
	private LocationRepository locationRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Location location = new LocationMock().createMock("A");
		when(locationRepository.save(Mockito.any(Location.class))).thenReturn(location);
		when(locationRepository.findByMapName(MAP)).thenReturn(new LocationMock().createMockList());
		when(locationBO.saveLocation(Mockito.any(Location.class))).thenReturn(new LocationMock().createMock(A));
		when(coordinateBO.saveCoordinate(Mockito.any(Coordinate.class))).thenReturn(new CoordinateMock().createMock());
		when(coordinateBO.findByMapName(MAP)).thenReturn(new CoordinateMock().createMockList());
	}

	@Test
	public void saveMap() {
		MapTO mapTO = new MapTO();
		ArrayList<CoordinateTO> coordinateTOList = new ArrayList<CoordinateTO>();
		coordinateTOList.add(new CoordinateTO());
		mapTO.setCoordinates(coordinateTOList);
		BaseResponseTO response = mapBO.saveMap(mapTO);
		Assert.assertEquals(MSG_SUCCESS, response.getMessage());
	}

	@Test
	public void cleanMap() {
		mapBO.cleanMap(MAP);
		Mockito.verify(locationBO, Mockito.timeout(1)).deleteByMapName(MAP);
		Mockito.verify(coordinateBO, Mockito.timeout(1)).deleteByMapName(MAP);
	}

	@Test
	public void calculateBetterRoute() {
		BetterRouteResponseTO betterRoute = mapBO.calculateBetterRoute(MAP, A, D, 2.50d, DEZ);
		Assert.assertEquals(Double.doubleToLongBits(6.25d), Double.doubleToLongBits(betterRoute.getBetterCost().doubleValue()));
	}
}
