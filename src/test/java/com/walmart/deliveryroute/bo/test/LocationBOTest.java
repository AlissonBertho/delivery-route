package com.walmart.deliveryroute.bo.test;

import static com.walmart.deliveryroute.mock.LocationMock.A;
import static com.walmart.deliveryroute.mock.LocationMock.B;
import static com.walmart.deliveryroute.mock.LocationMock.MAP;
import static org.apache.commons.lang.math.NumberUtils.INTEGER_ONE;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.walmart.deliveryroute.bo.LocationBO;
import com.walmart.deliveryroute.entity.Location;
import com.walmart.deliveryroute.mock.LocationMock;
import com.walmart.deliveryroute.repository.LocationRepository;

public class LocationBOTest {

	@InjectMocks
	private LocationBO locationBO;

	@Mock
	private LocationRepository locationRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Location location = new LocationMock().createMock("A");
		when(locationRepository.save(Mockito.any(Location.class))).thenReturn(location);
		when(locationRepository.findByMapName(MAP)).thenReturn(new LocationMock().createMockList());
	}

	@Test
	public void saveCoordinateTest() {
		Location location = new Location(B, MAP);
		Location locationResponse = locationRepository.save(location);

		Assert.assertEquals(locationResponse.getMapName(), MAP);
		Assert.assertEquals(locationResponse.getName(), A);
	}

	@Test
	public void findByMapNameTest() {
		List<Location> locations = locationRepository.findByMapName(MAP);
		Assert.assertNotNull(locations);
		Assert.assertTrue(!locations.isEmpty());
		Assert.assertEquals(locations.get(INTEGER_ONE).getName(), B);
	}
}
