package com.walmart.deliveryroute.mock;

import static com.walmart.deliveryroute.mock.LocationMock.A;
import static com.walmart.deliveryroute.mock.LocationMock.B;
import static com.walmart.deliveryroute.mock.LocationMock.C;
import static com.walmart.deliveryroute.mock.LocationMock.CINQUENTA;
import static com.walmart.deliveryroute.mock.LocationMock.D;
import static com.walmart.deliveryroute.mock.LocationMock.DEZ;
import static com.walmart.deliveryroute.mock.LocationMock.E;
import static com.walmart.deliveryroute.mock.LocationMock.MAP;
import static com.walmart.deliveryroute.mock.LocationMock.QUINZE;
import static com.walmart.deliveryroute.mock.LocationMock.TRINTA;
import static com.walmart.deliveryroute.mock.LocationMock.VINTE;

import java.util.ArrayList;
import java.util.List;

import com.walmart.deliveryroute.entity.Coordinate;

public class CoordinateMock {

	public Coordinate createMock() {

		return new Coordinate(new LocationMock().createMock(A), new LocationMock().createMock(B), DEZ, MAP);
	}

	public List<Coordinate> createMockList() {
		List<Coordinate> coordinates = new ArrayList<Coordinate>();
		coordinates.add(new Coordinate(new LocationMock().createMock(A), new LocationMock().createMock(B), DEZ, MAP));
		coordinates.add(new Coordinate(new LocationMock().createMock(B), new LocationMock().createMock(D), QUINZE, MAP));
		coordinates.add(new Coordinate(new LocationMock().createMock(A), new LocationMock().createMock(C), VINTE, MAP));
		coordinates.add(new Coordinate(new LocationMock().createMock(C), new LocationMock().createMock(D), TRINTA, MAP));
		coordinates.add(new Coordinate(new LocationMock().createMock(B), new LocationMock().createMock(E), CINQUENTA, MAP));
		coordinates.add(new Coordinate(new LocationMock().createMock(D), new LocationMock().createMock(E), TRINTA, MAP));
		return coordinates;
	}
}
