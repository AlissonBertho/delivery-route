package com.walmart.deliveryroute.mock;

import java.util.ArrayList;
import java.util.List;

import com.walmart.deliveryroute.entity.Location;

public class LocationMock {

	public static final String A = "A";
	public static final String B = "B";
	public static final String C = "C";
	public static final String D = "D";
	public static final String E = "E";
	public static final String MAP = "SP";
	public static final Double DEZ = 10.0d;
	public static final Double QUINZE = 15.0d;
	public static final Double VINTE = 20.0d;
	public static final Double TRINTA = 30.0d;
	public static final Double CINQUENTA = 50.0d;

	public Location createMock(String name) {
		return new Location(name, MAP);
	}

	public List<Location> createMockList() {

		List<Location> locations = new ArrayList<Location>();
		locations.add(new Location(A, MAP));
		locations.add(new Location(B, MAP));
		locations.add(new Location(C, MAP));
		locations.add(new Location(D, MAP));
		locations.add(new Location(E, MAP));

		return locations;
	}
}
